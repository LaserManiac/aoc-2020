use std::collections::HashMap;

fn main() {
    let start = std::time::Instant::now();
    
    let mut v = get_input();
    v.sort_by(|a, b| a.cmp(b).reverse());
    
    let mut c = HashMap::with_capacity(v.len());
    let mut s = Vec::with_capacity(v.len());
    let mut i = 1;
    let mut k = v[0];
    let mut n = (k <= 3) as usize;
    loop {
        if i >= v.len() || v[i] + 3 < k {
            if let Some((lk, li, ln)) = s.pop() {
                c.insert(li, n);
                k = lk;
                i = li + 1;
                n = ln + n;
            } else {
                break;
            }
        } else if let Some(c) = c.get(&i) {
            n += *c;
            i = i + 1;
        } else {
            s.push((k, i, n));
            k = v[i];
            i = i + 1;
            n = (k <= 3) as usize;
        }
    }

    let time = std::time::Instant::now() - start;
    println!("[{}us] {}", time.as_micros(), n);
}

fn get_input() -> Vec<u32> {
    INPUT.lines().filter_map(|l| l.parse().ok()).collect()
}

const INPUT: &str = r#"
99
104
120
108
67
136
80
44
129
113
158
157
89
60
138
63
35
57
61
153
116
54
7
22
133
130
5
72
2
28
131
123
55
145
151
42
98
34
140
146
100
79
117
154
9
83
132
45
43
107
91
163
86
115
39
76
36
82
162
6
27
101
150
30
110
139
109
1
64
56
161
92
62
69
144
21
147
12
114
18
137
75
164
33
152
23
68
51
8
95
90
48
29
26
165
81
13
126
14
143
15
"#;
