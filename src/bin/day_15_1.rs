use std::collections::HashMap;

fn main() {
    let mut turn = 1;
    let mut cache = HashMap::new();
    let mut input = INPUT.split(',').filter_map(|n| n.parse::<u32>().ok());
    let mut last = input.next().unwrap();
    
    for num in input {
        cache.insert(last, turn);
        turn += 1;
        last = num;
    }
        
    while turn < 2020 {
        let last_occured = cache.get(&last).copied();
        cache.insert(last, turn);
        match last_occured {
            Some(turn_occured) => last = turn - turn_occured,
            None => last = 0,
        }
        turn += 1;
    }
    
    println!("{}", last);
}

const INPUT: &str = r#"14,8,16,0,1,17"#;
