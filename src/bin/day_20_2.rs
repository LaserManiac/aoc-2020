use std::collections::HashMap;

fn main() {
    let (mut tiles, ordered) = get_input(INPUT);

    let mut edge_map = HashMap::<Edge, Vec<usize>>::new();
    for (id, tile) in &tiles {
        for edge in &tile.signature.edges() {
            edge_map.entry(*edge)
                .or_default()
                .push(*id);
        }
    }

    let mut min_x = 0;
    let mut max_x = 0;
    let mut min_y = 0;
    let mut max_y = 0;
    let mut x = 0;
    let mut y = 0;
    let mut dir = 0;
    let mut inv = false;
    let mut local_dir = 0;
    let mut curr = ordered.into_iter()
        .find(|idx| tiles[&idx].signature.edges()
            .iter()
            .filter(|e| edge_map[e].len() == 1)
            .count() == 2)
        .expect("No corner :[");

    let mut tile_positions = HashMap::new();
    tile_positions.insert(curr, (x, y));

    while tile_positions.len() < tiles.len() {
        let edge = tiles[&curr].signature.edges()[local_dir];

        let edge_tiles = &edge_map[&edge];
        let no_pair = edge_tiles.len() == 1;
        let new = if edge_tiles[0] == curr && !no_pair {
            edge_tiles[1]
        } else {
            edge_tiles[0]
        };

        if no_pair || tile_positions.contains_key(&new) {
            let dif = if inv { 3 } else { 1 };
            dir = (dir + 1) % 4;
            local_dir = (local_dir + dif) % 4;
            continue;
        }

        let (edge_dir, new_edge) = tiles[&new].signature
            .edges()
            .iter()
            .enumerate()
            .find(|(_, e)| **e == edge)
            .map(|(idx, e)| (idx, *e))
            .expect("Edge not found >:[");
        local_dir = (edge_dir + 2) % 4;
        let inverted = edge.hash == new_edge.hash;
        if inverted {
            inv = !inv;
        }

        match dir {
            0 => y -= 1,
            1 => x += 1,
            2 => y += 1,
            3 => x -= 1,
            _ => panic!("AYY FUCK"),
        }
        tile_positions.insert(new, (x, y));

        let rdiff = (dir + 4 - local_dir) % 4;
        let inv_x = inv && local_dir % 2 == 0;
        let inv_y = inv && local_dir % 2 == 1;
        tiles.get_mut(&new).unwrap().transform(rdiff, inv_x, inv_y);

        curr = new;

        min_x = min_x.min(x);
        max_x = max_x.max(x);
        min_y = min_y.min(y);
        max_y = max_y.max(y);
    }

    let width = (max_x - min_x + 1) as usize;
    let height = (max_y - min_y + 1) as usize;
    let mut map = vec![0; width * height];
    for (idx, (x, y)) in &tile_positions {
        let x = (*x - min_x) as usize;
        let y = (*y - min_y) as usize;
        map[x + y * width] = *idx;
    }

    let img_width = width * 8;
    let img_height = height * 8;
    let mut image = vec![false; img_width * img_height];
    for (i, idx) in map.into_iter().enumerate() {
        for j in 0..64 {
            let lx = j % 8 + 1;
            let ly = j / 8 + 1;
            let gx = i % width * 8 + j % 8;
            let gy = i / width * 8 + j / 8;
            image[gx + gy * img_width] = tiles[&idx].data[lx + ly * 10];
        }
    }

    let flip_configs = [
        (false, false),
        (false, true),
        (true, false),
        (true, true),
    ];
    let mut monster_count = 0;
    'outer: for (flip_x, flip_y) in flip_configs.iter().copied() {
        for rot in 0..4 {
            //println!("ROT: {}, FX: {}, FY: {}", rot, flip_x, flip_y);

            let width = if rot % 2 == 0 { MONSTER_WIDTH } else { MONSTER_HEIGHT };
            let height = if rot % 2 == 0 { MONSTER_HEIGHT } else { MONSTER_WIDTH };

            monster_count = 0;
            for i in 0..image.len() {
                let x = i % img_width;
                let y = i / img_height;
                if x >= img_width - width || y >= img_height - height {
                    continue;
                }

                if has_monster(x, y, &image, img_width, rot, flip_x, flip_y) {
                    monster_count += 1;
                    //println!("Monster @ ({}, {})", x, y);
                }
            }

            if monster_count > 0 {
                break 'outer;
            }
        }
    }

    let mc = MONSTER.chars().filter(|c| *c == '#').count();
    let tc = image.iter().filter(|f| **f).count();
    println!("{}", tc - mc * monster_count);

    /*for row in image.chunks(width * 8) {
        for val in row.iter() {
            print!("{}", if *val { '#' } else { '.' });
        }
        println!();
    }*/
    //println!("{}", sidx);
}


fn get_input(s: &str) -> (HashMap<usize, Tile>, Vec<usize>) {
    let mut ordered = Vec::new();
    let map = s.split("\n\n")
        .filter_map(|chunk| {
            let end = chunk.find('\n')?;
            let id = chunk["Tile ".len()..end - 1].parse().ok()?;
            let tile = Tile::new(&chunk[end + 1..]);
            ordered.push(id);
            Some((id, tile))
        })
        .collect();
    (map, ordered)
}


fn has_monster(
    x: usize,
    y: usize,
    map: &[bool],
    width: usize,
    rot: usize,
    flip_x: bool,
    flip_y: bool,
) -> bool {
    for (ly, line) in MONSTER.lines().enumerate() {
        for (lx, chr) in line.char_indices() {
            let lx = if flip_x {
                MONSTER_WIDTH - 1 - lx
            } else {
                lx
            };

            let ly = if flip_y {
                MONSTER_HEIGHT - 1 - ly
            } else {
                ly
            };

            let (lx, ly) = match rot {
                0 => (lx, ly),
                1 => (MONSTER_HEIGHT - ly, lx),
                2 => (MONSTER_WIDTH - lx, MONSTER_HEIGHT - ly),
                3 => (ly, MONSTER_WIDTH - lx),
                _ => panic!(">:["),
            };

            let x = x + lx;
            let y = y + ly;

            if chr == '#' && !map[x + y * width] {
                return false;
            }
        }
    }
    true
}


struct Tile {
    data: [bool; 100],
    signature: Signature,
}

impl Tile {
    fn new(s: &str) -> Self {
        let mut data = [false; 100];
        for (y, l) in s.lines().enumerate() {
            for (x, c) in l.char_indices() {
                data[x + y * 10] = c == '#';
            }
        }
        let signature = Signature::new(&data);
        Self { data, signature }
    }

    fn transform(&mut self, rot: usize, inv_x: bool, inv_y: bool) {
        let mut ndata = [false; 100];
        for i in 0..100 {
            let x = i % 10;
            let y = i / 10;
            let (nx, ny) = trpos(x, y, rot, inv_x, inv_y);
            ndata[nx + ny * 10] = self.data[i];
        }
        self.data = ndata;
    }
}


fn trpos(mut x: usize, mut y: usize, rot: usize, inv_x: bool, inv_y: bool) -> (usize, usize) {
    if inv_x {
        x = 9 - x;
    }

    if inv_y {
        y = 9 - y;
    }

    let (x, y) = match rot {
        0 => (x, y),
        1 => (9 - y, x),
        2 => (9 - x, 9 - y),
        3 => (y, 9 - x),
        _ => panic!(">:["),
    };

    (x, y)
}


#[derive(Copy, Clone, Eq, PartialEq)]
struct Signature {
    top: u16,
    right: u16,
    bottom: u16,
    left: u16,
}

impl Signature {
    fn new(data: &[bool; 100]) -> Self {
        let top = hash((0..10).into_iter().map(|i| data[i]));
        let right = hash((0..10).into_iter().map(|i| data[i * 10 + 9]));
        let bottom = hash((0..10).into_iter().map(|i| data[90 + i]).rev());
        let left = hash((0..10).into_iter().map(|i| data[i * 10]).rev());
        Self { top, right, bottom, left }
    }

    fn edges(&self) -> [Edge; 4] {
        [
            Edge::new(self.top),
            Edge::new(self.right),
            Edge::new(self.bottom),
            Edge::new(self.left),
        ]
    }
}


#[derive(Copy, Clone)]
struct Edge {
    hash: u16,
    min: u16,
    max: u16,
}

impl Edge {
    fn new(hash: u16) -> Self {
        let inv = inv(hash);
        Self {
            hash,
            min: hash.min(inv),
            max: hash.max(inv),
        }
    }
}

impl PartialEq for Edge {
    fn eq(&self, other: &Self) -> bool {
        self.min == other.min && self.max == other.max
    }
}

impl Eq for Edge {}

impl std::hash::Hash for Edge {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.min.hash(state);
        self.max.hash(state);
    }
}


fn hash(i: impl Iterator<Item=bool>) -> u16 {
    let mut x = 0;
    for val in i {
        x <<= 1;
        x |= val as u16;
    }
    x
}

fn inv(mut x: u16) -> u16 {
    let mut y = 0;
    for _ in 0..10 {
        y <<= 1;
        y |= x & 1;
        x >>= 1;
    }
    y
}

/*
..................#.
#....##....##....###
.#..#..#..#..#..#...
*/
const MONSTER: &str = "..................#.\n#....##....##....###\n.#..#..#..#..#..#...";

const MONSTER_WIDTH: usize = 20;
const MONSTER_HEIGHT: usize = 3;

const INPUT: &str = r#"Tile 2011:
.##...#..#
.#.#.#...#
.......###
.....##.#.
#...#.....
##...#...#
#.#.#....#
##..##....
.....#.#..
##.#......

Tile 3407:
#..#..#.#.
#..##....#
..#.......
......#...
......##.#
..#.#..#..
.#.#....##
##...#...#
#.#.##....
#.##..#...

Tile 3733:
##..###.#.
##.#.#..#.
.....###..
......#...
.##...##..
..##......
#.#.#.#..#
.#..##...#
....######
.#...#.#.#

Tile 2267:
..##.#..##
..#....#..
#..#...#..
.....##..#
##..##.###
#.#.....##
#.#...##..
#.......##
..#.....##
###.###.##

Tile 3853:
.####..#.#
.........#
......#..#
..#...#...
...#.....#
..........
....####.#
#........#
#....###..
.#..##...#

Tile 1531:
#.#..#....
.#..#.##..
.....#..#.
#....##...
#.#...#...
.#...###..
##.....#..
..##..#.#.
##....##..
###..###.#

Tile 3907:
#..##.##.#
##.##.##..
........#.
......##.#
#.##..#..#
#....#..#.
.##.##....
#.#.##....
.#..#.....
#.###...##

Tile 3329:
#.##..####
##....#..#
...#..#.#.
#..#..#..#
###..#...#
##.#.##..#
.#.##.....
##.#..####
.....#.#.#
#..#..#.##

Tile 3449:
...#.#..##
......#.##
#......##.
..##..#..#
...##.....
##.....##.
##..#.....
#...##...#
#.##...#.#
..#.#...##

Tile 1187:
#.#.......
...#.##.##
#.#.##...#
##..#....#
#..#..#..#
#.#.##...#
.....####.
..#...#...
.....#....
#.##...##.

Tile 1543:
.##.####.#
.##..#..#.
#..#.....#
###..#.#.#
.#.##.#.##
#...##.#..
#..#.#####
#...#.#..#
#..##...#.
..#..##..#

Tile 3631:
#..###...#
.#.##....#
#......###
#......#..
#.#......#
##..##...#
###.#..##.
...#..#..#
...#......
####.##..#

Tile 3917:
##.#######
.....##..#
..#..#...#
#......#..
##.#..###.
#....#.#..
.......##.
#....#..#.
.##....#..
#########.

Tile 1373:
####....#.
#......##.
.....#....
.##......#
#........#
#..##..#..
##..#.....
#......#..
.#..#.#..#
##...####.

Tile 2663:
..#.##....
.#..##..#.
.......#.#
##..#..#.#
..##.##.#.
....#.#..#
##.#.....#
#..#....##
###.#..###
...#.#....

Tile 3529:
...#.####.
.#.##....#
...#.##.##
#..#...#.#
#.#..#.#..
........#.
##........
....##...#
#..#....##
.###....##

Tile 2297:
...##....#
#...##...#
#......#..
##.......#
.#...#..##
.#....#.#.
#....##..#
#....###.#
##.......#
###..#..#.

Tile 2789:
..#.##.##.
#.#..#....
#......#.#
#......#.#
..#..#....
#...#.....
...#....#.
#......#.#
.....#...#
...#....#.

Tile 3461:
..###.####
##....####
#.###.#...
##......##
#.#......#
##......##
#..#...#..
.........#
......#.#.
#.#..#.###

Tile 2939:
##.####...
.......#.#
###.##..#.
.####....#
#.#....###
#.....#..#
##.....#..
..#....##.
#.#.......
.......#..

Tile 2903:
##.#.#.#.#
##.##..#..
.........#
#......##.
....#..###
###.#.#...
...#.....#
#.....#..#
##..#.....
....#.#.#.

Tile 1993:
.##..#####
....#....#
.#..#...##
...#......
...#..#...
#...#.##.#
#....#...#
#..#....#.
..#..#..#.
.###.#####

Tile 1669:
.##.##...#
###.###..#
###...##.#
##.#...###
#.#.#.####
.##....###
.####.#.#.
#.#.##...#
#.#..###..
....#.##.#

Tile 3823:
##.......#
###...##..
##.....##.
...##.##.#
..#......#
.##...#.##
.##.#..#..
###.#.....
......#...
.##.#....#

Tile 3929:
#....###.#
..........
.........#
#.###....#
...#.##...
.#.#.###..
#...##....
#..#.#..##
#......###
..####.#..

Tile 2521:
....##..#.
...###.#.#
.#....###.
#.#...#.#.
##....#...
#....##...
#...##.#.#
....##....
#.....####
#.#.##....

Tile 3361:
....##..#.
#.##.#...#
..##..#.##
#..#.#....
...##...#.
..#...###.
.#####...#
#.#####...
#.#..#..##
#.#####.##

Tile 1259:
..##....##
..#...#...
#.#.#..#.#
#........#
.##......#
##..##...#
###..###..
##....#..#
..###.#.#.
....#....#

Tile 2729:
.##..####.
#.##.#....
.....#.#.#
..#......#
#.#....#.#
..#..#..#.
##...####.
#...##....
.#.......#
..#..##.##

Tile 3511:
#...##.###
#...#.##.#
......##.#
#.........
#.##..#.#.
....#....#
...#.##...
##.##.###.
..#......#
#...##.#.#

Tile 3119:
.....##.##
#.....####
..#..#.#..
.#..##.#.#
.##.....#.
...#...#.#
..#.#.#..#
#.........
#.......#.
########.#

Tile 1427:
###..#.#..
#.#.....#.
.####...##
.#..#.....
..#.......
.#...#.#..
..#..##...
..##......
###..#.#..
...##.....

Tile 3307:
...#...#..
.###....#.
..#.#..#..
#......#.#
##.####...
#.##.#.##.
.##..###.#
...#......
....#.#.##
.#.####.##

Tile 2081:
.....####.
...#....##
#.#..#..##
#...#...#.
.#..##.###
...#.##.#.
#.......#.
##...#....
....#..#..
#.#...#.#.

Tile 1741:
##.##....#
#..##.##.#
.#..#..##.
.#.#..##.#
.###......
#.####....
..#..#.##.
####.#....
##....#...
#.#..#.#..

Tile 1481:
#.#...#...
#.#.##.###
#.......#.
#.#.#.....
.####...##
####....#.
#.#..#....
...#.#...#
........#.
#....####.

Tile 3719:
.###..#...
..#..##...
#..#...#.#
.##.#.#.##
..####.#..
#...###.##
.#..###...
...#.....#
#..#..#.##
.##.#.#.##

Tile 2797:
..##.#.#..
....#....#
..###...##
..........
.......#.#
.........#
#.#.......
####.####.
#.#..##.#.
.#.#...#.#

Tile 1319:
..####.###
.#.##.#.#.
##.##.#.##
#.##..##..
#..##..##.
.##.#..#..
#.#..#...#
..#.#...##
#..#..###.
#.###..###

Tile 2053:
.##....###
##..#.....
#.#...#.#.
.........#
.#.#..##..
..#..##.##
#.##.#.#..
......#..#
#.#.###..#
#...###.##

Tile 1867:
.#.###..#.
#...#..#..
#.....##..
#....#..#.
#.......##
#...#....#
.#..####.#
###......#
.##.##.##.
.##.##....

Tile 2131:
##.##..#.#
##..####.#
...####...
#....##..#
..##....##
##....#...
..#..##..#
.###..##..
..#..#...#
....#..###

Tile 1777:
###..##...
#.#.#...##
#..##....#
.........#
#..#..#..#
#...#.#..#
#..#......
.#..#.#..#
##....#..#
....##.#.#

Tile 3037:
##..#..#..
##......#.
...###...#
.####....#
#..#......
#...#.#...
..#####.#.
#..##.....
......#...
..##.#.###

Tile 2749:
..###.##..
####.....#
#..####.#.
#...#....#
#.........
#.......##
.####..##.
..##..###.
#.##....#.
#.#...#...

Tile 3919:
#.#....##.
.#....#.##
......##..
.#....##..
#.......#.
#..#...#..
#.#..###..
##...##..#
..#..##.#.
.####.#...

Tile 1231:
.#.#.#####
........#.
##...#..##
....#....#
.......#.#
.#.......#
...#..#.#.
.........#
...##.#.#.
####.#.###

Tile 2441:
##.......#
##.....##.
.#......##
#.###...##
#.......#.
....#...#.
#.#...##..
##.....#..
.#.#.#.###
#########.

Tile 3659:
....#.#...
##.#####.#
...#....##
.#.##..#.#
#.##.....#
#.#...#...
#.....#...
......#...
#.......#.
###...####

Tile 2411:
.##...#.#.
#..#.##..#
#....##...
#...#.....
#......#.#
#........#
.#.##.....
#...#..#..
...#.##.#.
##.#...#.#

Tile 1999:
...###....
.##.......
.#...##.#.
..#......#
#.......##
###...####
.#......##
.###....##
..##..##..
##..#.#.##

Tile 3623:
###....#..
....##.#..
..#...#...
.#......##
#..#..#...
#.#####...
....##...#
.#..#.....
#..#.#.#.#
#....#...#

Tile 2851:
#.##.##.##
##..###...
...#.##.#.
.....#....
##....#..#
#.##....##
##.#.####.
#..#..#...
...##....#
...##..#.#

Tile 3673:
.#####.#..
#.....#.##
.#........
..#..#....
.##..#....
#.........
#.##..##..
......#..#
#.......#.
###..##..#

Tile 2399:
##..#....#
#.######..
.#.#.#####
##.#..#...
.#....#..#
###..###.#
#..#..#.##
#..##....#
.##....#..
#.#....##.

Tile 3727:
#..##.#...
###....#.#
.##..##.##
.#...#..#.
..#.....#.
.#.#.....#
####.....#
..#..#....
..##..#..#
###.###..#

Tile 2311:
#..#...#..
..#.....##
....#.#..#
..#....##.
.....#.#.#
#........#
..#...##.#
#.#.#...##
##.#.#..#.
..#...#...

Tile 1009:
.#.###.##.
....#...#.
#....#....
#.##...##.
#....#.#.#
#.###.....
.##..#.##.
.#..#.##.#
##..#....#
.###.##.##

Tile 3313:
##.#..####
.#...#..##
..#....#..
...#...#.#
######....
...#......
..........
.#...#.##.
...#.#....
###.#.#.##

Tile 1733:
.#.#.###..
.##...##.#
###..#.#.#
#.#...##..
#.....##.#
##........
##.##.#..#
.#....#.#.
####.#.##.
####....#.

Tile 3947:
.####...##
.....#....
#.#.......
.....#...#
..#....#..
#...#..#..
...#......
##.#....#.
..##..#...
..#.#.#.##

Tile 1583:
#..###....
....#.#..#
...##.#..#
.##....##.
.......###
#.......##
...#.##..#
##.....#.#
###..#..#.
#...##....

Tile 3911:
.###...###
.#.......#
###.#....#
.###......
..#.#.#...
....#.#...
#..#...#..
#...#..#.#
.#.#..#..#
.#.#....#.

Tile 2833:
.#####...#
..##....##
#####.....
.#..##..##
#.#..#.#..
#..###.#..
#.#.##...#
...#..#..#
...#..##..
#.#.......

Tile 1117:
...#####.#
.##...#...
.........#
##......#.
....#..#.#
.###..#.##
###..##...
..##.#...#
#..#.##..#
.#..###...

Tile 2693:
...#.#...#
##..#...##
.#.....##.
...#.#....
.###.....#
...#......
##.......#
#........#
....#.....
..#..###..

Tile 3527:
..##.##...
.#.#......
##..#...##
#..##.#...
#...#...#.
#.#.#....#
#..##..##.
........#.
......##.#
.#####...#

Tile 2417:
.###.#...#
....#....#
###....###
..........
.#...###..
.#####.##.
#.#.....##
.....#...#
..#..#.#.#
###.##.#.#

Tile 1723:
..#####.##
#..##..#..
#..##...#.
..#.....##
..#...#...
....#...##
....#.....
#...#....#
...#..###.
.##.##....

Tile 2251:
.....#####
..#......#
#....#..#.
.#.##.#..#
.....#..##
........##
..........
.#...#...#
.#..#..#..
#.#..##.#.

Tile 3761:
#.#.#...#.
#.#.#.#.#.
#..#....#.
#.........
##.##..#.#
...#......
##..#.....
#...###.#.
..###....#
..##.##...

Tile 1697:
.#...#....
..##..#...
#........#
##..#...##
......##.#
...##...#.
..##...#..
.####....#
..#......#
##.#.#.#.#

Tile 1879:
..#...#.#.
#.#.....##
..#..#..##
.......#.#
#...#...#.
........##
##.#.....#
..###...#.
.....#.#..
#....#..##

Tile 1901:
##.#.#.###
.....#....
##.#.....#
#.#.#.#...
.#........
####.#...#
#.#...##.#
.##.##.##.
..##...##.
#.#...##..

Tile 1063:
..#######.
.....#.##.
...#......
#........#
#........#
#.#..##...
...#...#.#
###...#..#
#.#.....#.
.###..#.##

Tile 2389:
.###.###.#
...#...#..
..#.....#.
...#..#...
.#...#....
#...##..#.
#...##.#..
#.#.#.....
##...#..#.
##.#.#..#.

Tile 2393:
..#..#.#..
..#..##...
....#.#...
#..#.....#
##........
#.##...#..
....##...#
...##....#
...##.#.##
#.#.#.####

Tile 3067:
.....##..#
.#........
#..##..#..
#.#...#.##
#..###.#.#
#...#...##
#.##.#..##
...##...#.
.##...#.##
#.....#...

Tile 1747:
.#######.#
#...##....
....###..#
##...#.#..
#..#.#..#.
#..#.#.#.#
..###.##..
#.#.##..##
..........
###.#####.

Tile 3797:
..#######.
#.#.#.....
..........
#..##.##..
##..##.#..
#.#.#...#.
.###.#..#.
##...##.#.
#..##.##.#
.##.#.##.#

Tile 1811:
##..###...
.......##.
......#..#
..##....#.
##..######
#...#...#.
####......
#..#.##...
#..##....#
###.#...##

Tile 1087:
.##.##..#.
.#.##...##
.#......##
.#.....##.
...#.#.##.
###.#..###
###.#...##
####..#...
##...#...#
...#.#...#

Tile 1181:
...##...##
....#..##.
.#...#..##
##.#...#.#
..#.#.#.##
##.......#
#...#.###.
#.#.....#.
.#......##
..#.#.#..#

Tile 3821:
.#...#.#..
..#....###
..##.#..#.
#...#.#..#
#.###...#.
..##..##..
#.#.#..#.#
....####.#
#.........
..#.#.....

Tile 3457:
#..#....##
#........#
#.#..#.#.#
##.....#..
.....###.#
.#.##....#
#.....#..#
##...#...#
.#.....#.#
##.##...##

Tile 1657:
###..#.###
....#..###
..#...#..#
...###..#.
.....####.
#......#..
.##...#.#.
.#.#.#.##.
....#...#.
...#.##.#.

Tile 2503:
.#....##..
#.#...#...
##..####..
.#.#.....#
#.#..##...
...###...#
...##....#
.....#...#
.....##...
..#...####

Tile 2843:
....#####.
#.......##
#...#.##..
.#.#.#...#
..#.##...#
..#..#.###
.......#..
....#..###
......#.#.
#.####.##.

Tile 2273:
#....##..#
##.#....#.
#.###...#.
#.........
...#......
####......
#.##.....#
#..##..#..
#.......##
.#..#..###

Tile 2953:
.#.#.###..
#...##.#..
.#........
....#....#
...##..##.
#.....#...
#.....#...
#...#.##..
.#......#.
###..#..##

Tile 1021:
##.#.###.#
##....##..
#......#..
...#...#.#
#.##.....#
......#...
#..#......
.##...#...
#...#...##
.##...#..#

Tile 3643:
#...#.....
...#...#..
......#.##
.....###..
.....#.###
.....#.#..
.......##.
##..#.#...
#......#..
..#.####..

Tile 1609:
##.#.#..##
..........
#.#..#.#..
####...###
.#...#..#.
##....##..
#.#.##..##
#...##....
##....#.#.
###..#.#..

Tile 1949:
#.#.#.##..
#.#.......
#.#......#
###.#....#
...#.##...
#...#.....
.#...#....
...##.#...
.#..#....#
.##..###.#

Tile 1153:
.##.##..#.
..#......#
#.#.....#.
##.##..##.
#.#......#
....#....#
.#.##....#
#.#..#...#
..#...#.##
#.#.##..##

Tile 1367:
.###.#####
.........#
#.##....##
....##..##
.#.#.....#
####..##..
..#.###..#
.#..##.#.#
..#.....##
#...##.###

Tile 1061:
..####....
.......#.#
#.....#...
#..#.##..#
#.##....#.
.#...#.#.#
###.....##
..#...#...
...#.##.##
.#....#...

Tile 1193:
...#....##
...###..##
#..##....#
#....#....
#...#..#.#
..##.##..#
.#.###...#
##....#..#
#...#..##.
####.#....

Tile 3617:
#..#.#..#.
#.#.#.##..
#.##.#..##
#.#.#.....
###......#
##....#..#
.....##.#.
#......###
...###..##
.##.#..#.#

Tile 1093:
#..##...#.
.#.#.#...#
#........#
.#...##..#
....#.#.##
#.....#.##
#........#
...#..#...
#.##..##..
.#...#..##

Tile 1549:
##..#.###.
..#......#
#.#..#..#.
........##
....#..#.#
#.#.#.#...
...##..#..
######...#
#....##..#
.####..#.#

Tile 2281:
###.######
.#.#......
#.#..##...
.#.#...###
..#...#..#
..##.....#
.......#..
#....##..#
....#..#..
..####..##

Tile 3049:
.....##...
#..#......
#....##...
#...#.#...
###...##.#
##....#...
##...##..#
#...#.....
.#.#.##...
######...#

Tile 1523:
##..###.#.
.......#..
##..####..
.#...#...#
.....#..#.
.##.#.#..#
##.#.....#
###..#..#.
...##.....
##.##.....

Tile 3671:
.#.#.###.#
.........#
...#..#...
...#.#....
.#...#.###
###.#.#.##
..#......#
..........
#..##.###.
#.##.####.

Tile 3221:
...##.##.#
#....#...#
##..###...
#...##.#..
##........
##.....#..
###.......
.......#.#
#.#..###.#
#..#.#...#

Tile 1861:
.....##..#
##...#.#.#
#...#....#
....##...#
#..##..#.#
...#.#...#
##........
#.#.#.#...
#.........
##......#.

Tile 2111:
#....##.##
..#....#.#
......##.#
#..#..#...
..#..##.#.
##..###.#.
.#..#.#..#
.#.#...#..
#.##.#...#
#.#..##..#

Tile 1049:
..#.######
.#......#.
.........#
....##..#.
.#....#..#
....##..##
..#......#
#..#.#...#
##........
.#.##..##.

Tile 1871:
.##.##.#.#
#...#.....
#..#.#..##
.###..#.##
.....#....
#....#.#.#
#..##....#
.#.##....#
.#...##.##
.##.#.####

Tile 3389:
...###..#.
.#...#..#.
###.......
.#...#.#.#
.#.#.#.#.#
#..##.###.
..###..##.
#...#.....
#.#...#...
##.##.#.##

Tile 2699:
.#..#.####
#...#.#.#.
##.....#.#
#....#...#
..#...#...
...###..##
#.#.##..#.
...###.##.
#....##.#.
#..#..#.#.

Tile 3181:
#.##.#..##
#......##.
........#.
##.###....
##.......#
#.........
##...#...#
#.#.#....#
...#.....#
#..###....

Tile 3469:
..##...#.#
...##..#.#
##.##.#.##
.#####.###
..#......#
.#......##
..#..####.
##.#.#..#.
.#......##
#..##.#...

Tile 1249:
##.###...#
.#........
....#..#.#
#.....#..#
#.###..#..
.#.......#
.....#.#..
##....#...
.#...##..#
.#..###.##

Tile 1907:
.####.####
#....###.#
...#.#..#.
..#..#..##
#..##...##
#.#.......
#...##...#
#..#...#..
.#...###..
...###.##.

Tile 2539:
...#.##.#.
##.....#.#
#..##.....
#####.....
#..#.#..#.
###..#....
....#..#.#
.......#.#
#.#.....#.
#..#####.#

Tile 1217:
.#.###.###
.........#
#.....#...
##..#.#...
..#.#.....
......##.#
.#.......#
#.#.#.#...
...###.#..
..####...#

Tile 2377:
.#...##..#
..#...#..#
........##
.........#
.#....#...
.###.....#
#....#....
.#...###.#
...#..#..#
###.#..###

Tile 2767:
.####..#..
##....#..#
#..#....#.
#..#.#####
###....#..
..#..#.###
#...#.....
#.#......#
.#...#..#.
#.#.#.####

Tile 2879:
#.....#..#
.#..#..#.#
##....##..
..........
#...#...#.
##.......#
#.###.#..#
.......##.
....####..
..##...##.

Tile 2683:
.##..##...
.#........
#..#.##..#
.#.#..##.#
........##
#.#......#
....#.##.#
#.##.#..#.
#.#.#...#.
#...##.#.#

Tile 2003:
.#.#.###.#
#..##.#...
##...##...
.#...##..#
.#..##.##.
#...##...#
#...#.#...
.#...#.##.
..........
######....

Tile 1511:
#..#.#..##
......#...
#.#.#..#.#
##..#....#
##......#.
#.........
.....#.#..
..##......
...##...##
..##...###

Tile 3967:
.##..###.#
....#....#
#.##...#.#
#......#.#
#.....####
..##....#.
#.#.#....#
....#.....
##.#.....#
#.###.###.

Tile 3343:
##.#.#....
.......#.#
..#......#
..##.#..#.
##....##.#
.#.#......
...#..#.##
#..#.##..#
.#.#.#.#..
..##..#..#

Tile 2269:
##........
#........#
.........#
...##..###
.###.##.##
#....##..#
.#..#.##.#
##.#...#..
##....#.##
..#.#....#

Tile 2063:
#...#.##.#
##.......#
.#..#.....
.##...###.
.#.....#..
..#.....#.
..#####.#.
#####...##
##.#...##.
.#.....##.

Tile 2857:
...##.####
##......##
#.#.#.#...
.#.###....
......#...
.........#
.#......##
.#.#.....#
#.#.#.....
..##.#.#..

Tile 3167:
.....#....
...###....
...##.....
.....#..##
..#.###..#
#..#....#.
..####....
.......#..
#...##..##
#...###...

Tile 2551:
.#.###....
#.##..#.#.
..###.#.#.
#..####.##
.......##.
..##.#....
.#..#....#
####....#.
#.##..###.
#....#.###

Tile 2707:
..###..#.#
.....#.###
#....#...#
##....##.#
...#..#.##
.##.#..##.
#...#.#.#.
#.....#..#
..##.##.#.
######.#.#

Tile 3607:
...#..#.#.
#.#..#..##
..##...#.#
##.#.....#
##...##...
#.#..#....
#........#
.#..##..##
####...###
##.#..####

Tile 2113:
....#.....
.....#...#
...#..#..#
##..#.#...
...##.....
#.##..#..#
.....#..##
#..#.#....
#.#.###...
.#.####..#

Tile 2371:
##.##...##
#.#......#
##.#......
#......#.#
....#....#
##...#.#.#
#...#...##
#....#...#
..#..#...#
......#..#

Tile 2741:
###..#####
#..#..##.#
##...##...
##........
...##...#.
.##.....#.
.#..#.#.#.
#.#......#
..##....##
.###......

Tile 3779:
..#.....#.
##.......#
..#.#.##.#
.###.#.##.
.#..####..
#####.#..#
...#.....#
#...#.....
.........#
.##...#.#.

Tile 1483:
##.#..##..
#.#....#.#
..........
....#.#..#
#...#....#
#....#...#
..##.#..#.
#..###.###
..##.#....
..#.##....

Tile 2927:
####.#####
.###......
#..##..#.#
##...#....
##.#..#...
.##.#.....
.#........
..........
#........#
.##....#..

Tile 2153:
#.#..#####
#..#...#..
#.......##
##..#.####
....##.#.#
#.#.##....
.#..##.#..
..#.##..#.
#.#####..#
...#.##.##

Tile 1453:
.....###..
...#......
.#.#..#..#
.#..#...##
.####..#..
...#..##..
....#.#.##
.##....##.
##......#.
.##.#...##

Tile 2423:
#..##.###.
##...##..#
#.........
.#...#..##
#.........
.....##...
#...#.....
.......#..
#....#....
..##...###

Tile 1933:
#.##.###.#
##....#...
#..##...#.
.......#..
##....#..#
....#..##.
#..#......
.#........
..#.#.....
##....###.

Tile 3191:
.#####.#.#
##.#.....#
....#...#.
##.#......
...#...##.
..#..#...#
#..#..#...
#..#......
..#.######
.###..#..#"#;
