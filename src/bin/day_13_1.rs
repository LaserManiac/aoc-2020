fn main() {
    let (t, mut b) = get_input().expect("Bad input :[");
    
    let mut bid = b.next().expect("No busses :[");
    let mut bwt = wait_time(t, bid);
    for id in b {
        let wt = wait_time(t, id);
        if wt < bwt {
            bid = id;
            bwt = wt;
        }
    }
    println!("{}", bid * bwt);
}

fn wait_time(t: u64, id: u64) -> u64 {
    ((t - 1) / id) * id + id - t
}

fn get_input() -> Option<(u64, impl Iterator<Item=u64>)> {
    let mut i = INPUT.lines().filter(|l| !l.is_empty());
    let t = i.next()?.parse::<u64>().ok()?;
    let b = i.flat_map(|l| l.split(',').filter_map(|b| b.parse::<u64>().ok()));
    Some((t, b))
}

const INPUT: &str = r#"
1003055
37,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,41,x,x,x,x,x,x,x,x,x,433,x,x,x,x,x,x,x,23,x,x,x,x,x,x,x,x,17,x,19,x,x,x,x,x,x,x,x,x,29,x,593,x,x,x,x,x,x,x,x,x,x,x,x,13
"#;
