use std::collections::{HashMap, HashSet};

fn main() {
    let mut points = get_input(INPUT);
    for _ in 0..6 {
        let mut ncounts = HashMap::with_capacity(points.len());
        for point in &points {
            for neighbour in point.neighbors() {
                *ncounts.entry(neighbour).or_insert(0usize) += 1;
            }
        }
        
        let mut next = HashSet::with_capacity(points.len());
        for (point, ncount) in ncounts {
            if points.contains(&point) {
                if ncount == 2 || ncount == 3 {
                    next.insert(point);
                }
            } else if ncount == 3 {
                next.insert(point);
            }
        }
        points = next;
    }
    
    println!("{}", points.len());
}


#[derive(Clone, Copy, Eq, PartialEq, Hash)]
struct Point {
    x: i64,
    y: i64,
    z: i64,
}

impl Point {
    fn neighbors<'a>(&'a self) -> impl Iterator<Item=Point> + 'a {
        (self.x - 1..self.x + 2).into_iter()
            .flat_map(move |x| (self.y - 1..self.y + 2).into_iter()
                .flat_map(move |y| (self.z - 1..self.z + 2).into_iter()
                    .filter_map(move |z| {
                        if x != self.x || y != self.y || z != self.z {
                            Some(Point { x, y, z })
                        } else {
                            None
                        }
                    })))
    }
}


fn get_input(s: &str) -> HashSet<Point> {
    s.lines()
        .enumerate()
        .flat_map(|(y, l)| l.char_indices()
            .flat_map(move |(x, c)| {
                if c == '#' {
                    Some(Point {
                        x: x as _,
                        y: y as _,
                        z: 0,
                    })
                } else {
                    None
                }
            }))
        .collect()
}


const INPUT: &str = r#"
##.#####
#.##..#.
.##...##
###.#...
.#######
##....##
###.###.
.#.#.#..
"#;
