use std::collections::VecDeque;

fn main() {
    let mut d1 = VecDeque::new();
    let mut d2 = VecDeque::new();

    let mut curr = &mut d1;
    for line in INPUT.lines() {
        if line.starts_with("Player 1") {
            curr = &mut d1;
        } else if line.starts_with("Player 2") {
            curr = &mut d2;
        } else if !line.is_empty() {
            let n = line.parse::<usize>().unwrap();
            curr.push_back(n);
        }
    }

    while d1.len() > 0 && d2.len() > 0 {
        let c1 = d1.pop_front().unwrap();
        let c2 = d2.pop_front().unwrap();
        let w = if c1 > c2 {
            &mut d1
        } else if c2 > c1 {
            &mut d2
        } else {
            panic!("NEIN >:[");
        };
        w.push_back(c1.max(c2));
        w.push_back(c1.min(c2));
    }

    let w = if d1.len() > 0 { &d1 } else { &d2 };
    let n: usize = w.iter()
        .rev()
        .enumerate()
        .map(|(idx, c)| (idx + 1) * *c)
        .sum();
    println!("{}", n);
}

const INPUT: &str = r#"Player 1:
14
6
21
10
1
33
7
13
25
8
17
11
28
27
50
2
35
49
19
46
3
38
23
5
43

Player 2:
18
9
12
39
48
24
32
45
47
41
40
15
22
36
30
26
42
34
20
16
4
31
37
44
29"#;
