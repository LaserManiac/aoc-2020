use std::collections::{HashSet, VecDeque};

fn main() {
    let mut d1 = VecDeque::new();
    let mut d2 = VecDeque::new();

    let mut curr = &mut d1;
    for line in INPUT.lines() {
        if line.starts_with("Player 1") {
            curr = &mut d1;
        } else if line.starts_with("Player 2") {
            curr = &mut d2;
        } else if !line.is_empty() {
            let n = line.parse::<usize>().unwrap();
            curr.push_back(n);
        }
    }
    
    let (_, w) = play(d1.clone(), d2.clone());
    let n: usize = w.iter()
        .rev()
        .enumerate()
        .map(|(idx, c)| (idx + 1) * *c)
        .sum();
    println!("{}", n);
}

// false = P1
// true = P2
fn play(mut d1: VecDeque<usize>, mut d2: VecDeque<usize>) -> (bool, VecDeque<usize>) {
    let mut seen = HashSet::new();
    
    while d1.len() > 0 && d2.len() > 0 {
        if seen.contains(&(d1.clone(), d2.clone())) {
            return (false, d1);
        }
        
        seen.insert((d1.clone(), d2.clone()));
        
        let c1 = d1.pop_front().unwrap();
        let c2 = d2.pop_front().unwrap();
        
        if c1 > d1.len() || c2 > d2.len() {
            if c1 > c2 {
                d1.push_back(c1);
                d1.push_back(c2);
            } else if c2 > c1 {
                d2.push_back(c2);
                d2.push_back(c1);
            }
        } else {
            let nd1 = d1.iter().take(c1).copied().collect::<VecDeque<_>>();
            let nd2 = d2.iter().take(c2).copied().collect::<VecDeque<_>>();
            let (res, _) = play(nd1, nd2);
            if !res {
                d1.push_back(c1);
                d1.push_back(c2);
            } else {
                d2.push_back(c2);
                d2.push_back(c1);
            }
        }
    }

    if d1.len() > 0 {
        (false, d1)
    } else {
        (true, d2)
    }
}

const INPUT: &str = r#"Player 1:
14
6
21
10
1
33
7
13
25
8
17
11
28
27
50
2
35
49
19
46
3
38
23
5
43

Player 2:
18
9
12
39
48
24
32
45
47
41
40
15
22
36
30
26
42
34
20
16
4
31
37
44
29"#;
