fn main() {
    let mut buses = get_input().expect("Bad input :[");
    let (offset, mut period) = buses.pop().expect("No buses :[");
    let mut time = period - offset;
    while let Some((offset, id)) = buses.pop() {
        while (time + offset) % id != 0 {
            time += period;
        }
        period = lcm(period, id);
    }
    println!("{}", time);
}

fn gcd(mut a: usize, mut b: usize) -> usize {
    loop {
        if b != 0 {
            let t = b;
            b = a % b;
            a = t;
        } else {
            break a;
        }
    }
}

fn lcm(a: usize, b: usize) -> usize {
    a * b / gcd(a, b)
}

fn get_input() -> Option<Vec<(usize, usize)>> {
    INPUT.lines()
        .filter(|l| !l.is_empty())
        .skip(1)
        .flat_map(|l| l.split(',')
            .enumerate()
            .filter_map(|(i, b)| Some((i, b.parse().ok()?))))
        .collect::<Vec<_>>()
        .into()
}

const INPUT: &str = r#"
1003055
37,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,41,x,x,x,x,x,x,x,x,x,433,x,x,x,x,x,x,x,23,x,x,x,x,x,x,x,x,17,x,19,x,x,x,x,x,x,x,x,x,29,x,593,x,x,x,x,x,x,x,x,x,x,x,x,13
"#;
